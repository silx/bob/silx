# silx

[![pipeline status](https://gitlab.esrf.fr/silx/bob/silx/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/silx/pipelines)

Build binary packages for [silx](https://github.com/silx-kit/silx): [Artifacts](https://silx.gitlab-pages.esrf.fr/bob/silx)